# LAMP Centos 7

```bash
$ vagrant up
$ ansible-galaxy install -r requirements.yml --force
$ ansible-playbook lamp-centos.yml
```